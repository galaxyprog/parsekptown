﻿using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace XPath
{
    class Program
    {
        static void ExtractProp(Dictionary<string, object> prop, XmlNode root, string path)
        {
            if (root.FirstChild.Name == "#text")
            {
                prop.Add(path, root.FirstChild.Value);
                return;
            }
            if (root.Name == "contours_location")
            {
                return;
            }

            foreach (XmlNode node in root)
            {
                //path += "." + node.Name;
                //ExtractProp(prop, node, path);
                //path = path.Substring(0, path.LastIndexOf('.'));


                ExtractProp(prop, node, path + "." + node.Name);
            }
        }

        static void ParseToJsons(string fn, string dirOut)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(fn);
            XmlElement xRoot = xDoc.DocumentElement;

            var landRecords = xRoot.SelectNodes("//land_records/land_record");
            List<Feature> records = new List<Feature>();
            foreach (XmlNode record in landRecords)
            {
                var elems = record.SelectNodes(".//spatial_element");
                List<LineString> polygons = new List<LineString>();
                foreach (XmlNode s in elems)
                {
                    var ords = s.SelectNodes(".//ordinate");
                    List<Position> coordinates = new List<Position>();
                    foreach (XmlNode ord in ords)
                    {
                        string x = ord.SelectSingleNode(".//x").InnerText;
                        string y = ord.SelectSingleNode(".//y").InnerText;
                        coordinates.Add(new Position(y, x));
                    }
                    polygons.Add(new LineString(coordinates));
                }
                if (polygons.Count != 0)
                {
                    var polygon = new Polygon(polygons);

                    Dictionary<string, object> properties = new Dictionary<string, object>();

                    ExtractProp(properties, record, "land_record");

                    var model = new Feature(polygon, properties);
                    records.Add(model);
                }
            }
            if (records.Count != 0)
            {
                var resKPT = new FeatureCollection(records);
                var retData = JsonConvert.SerializeObject(resKPT, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(Path.Combine(dirOut,Path.GetFileNameWithoutExtension(fn)) + ".geojson", retData);
            }
            else System.Console.WriteLine("Файл:" + fn + " не удалось распарсить!");
        }

        static void ConvertToJsons(string dirPath, string dirOut)
        {
            var dirs = Directory.GetDirectories(dirPath);
            foreach (var dir in dirs)
            {
                var files = Directory.GetFiles(dir, "*.xml");
                foreach (var file in files) 
                {
                    ParseToJsons(file, dirOut);
                }
            }
            var filesRoot = Directory.GetFiles(dirPath, "*.xml");
            foreach (var file in filesRoot)
            {
                ParseToJsons(file, dirOut);
            }
        }

        static void PereborDir(string dirPath, string dirOut)
        {
            var dirs = Directory.GetDirectories(dirPath);
            if (dirs.Length != 0)
            {
                foreach (var dir in dirs)   
                {
                    PereborDir(dir, dirOut);
                    var files = Directory.GetFiles(dir, "*.xml");
                    foreach (var file in files)
                    {
                        ParseToJsons(file, dirOut);
                    }
                    //System.Console.WriteLine(dir);
                }
            }
            var filesRoot = Directory.GetFiles(dirPath, "*.xml");
            foreach (var file in filesRoot)
            {
                ParseToJsons(file, dirOut);
            }
        }

        static void PereborDir1(string dirPath)
        {
            var dirs = Directory.GetDirectories(dirPath);
            if (dirs.Length != 0)
            {
                foreach (var dir in dirs)
                {
                    PereborDir1(dir);
                    System.Console.WriteLine(dir);
                }
            }
        }

        static void Main(string[] args)
        {
            //Укажите путь до папки с файлами КПТ с расширением xml, а вторым аргументом путь до папки в которую запишем результат


            //ConvertToJsons(@"D:\check", @"D:\out1\");
            //PereborDir(@"D:\КПТшки", @"D:\out1");
            PereborDir(@"E:\kptparser\05_09_000045_2020-11-03_kpt11", @"E:\kptparser\out");
            //PereborDir1(@"E:\kptparser\XPath");
        }
    }
}
